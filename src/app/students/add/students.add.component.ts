import { Component } from '@angular/core';
import Student from '../../entity/student';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent {
  model: Student = new Student();
  // TODO remove when done
  get diagnostic() { return JSON.stringify(this.model); }
  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }

}
